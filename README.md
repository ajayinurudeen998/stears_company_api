# Stears Business Interview Solution

## Setting up
    1. To get started, first create your .env file.
    2. Check or duplicate the content of .env.example and set the parameters.
    3. Start the mongodb server.
    4. Afterwards, navigate via the index.js file (this file serves as the main file/ passage to all other route configuration and app setup using express)
    5. Navigate to the router.js file for evaluating necessary routes.

Thank you.

## Usage

## License
[MIT](https://choosealicense.com/licenses/mit/)
