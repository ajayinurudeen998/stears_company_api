const mongoose = require("mongoose");

const { Schema } = mongoose;

const schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true
    },
    period: {
      type: String,
      required: true
    },
    year: {
      type: Number,
      required: true
    },
    assignee: {
      type: String,
      required: true
    },
    deadline: {
        type: Date,
        required: true
    },
    submitted: {
        type: Boolean,
        required: true,
        default: false,
    },
    url: {
        type: String,
        required: true
    },
    company : {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
  },
  {
    timestamps: true
  }
);

const Report = mongoose.model("Report", schema);
module.exports = Report;
