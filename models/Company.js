const mongoose = require("mongoose");

const { Schema } = mongoose;

const schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique : true,
      validate: {
        validator: (email) => Promise.resolve(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)),
        message: 'Please enter a valid email address'
      }
    },
    address: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    website: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    reports: [{ 
      type: Schema.Types.ObjectId, 
      ref: 'Report'
    }],
  },
  {
    timestamps: true
  }
);

const Company = mongoose.model("Company", schema);
module.exports = Company;
