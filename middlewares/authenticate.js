const jwt = require("jsonwebtoken");

const authenticateUsers = (req, res, next) => {
  var token =
    req.body._token || req.query._token || req.header('Authorization');

  if (token) {
    jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: `Failed to authenticate token. - ${err}`
        });
      }
      
      req.decoded = decoded;
      User.findByID(decoded.id, (err, user) => {
        if (err || !user) {
          return res.json({
            success: false,
            message: "Failed to authenticate token."
          });
        }
        req.user = user;
        next();
      });
    });
  } else {
    return res.status(403).send({
      success: false,
      message: "No token provided."
    });
  }
};

module.exports = authenticateUsers;
