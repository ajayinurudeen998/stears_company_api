const express = require("express");

const ReportController = require("./controllers/ReportController");
const CompanyController = require("./controllers/CompanyController");
const router = express.Router();

/**
    ROUTE GROUPS & CATEGORIES
*/

/**
 * @swagger
 * /api/puppies:
 *   post:
 *     tags:
 *       - Companies
 *   parameters:
 *       - in: body
 *       name: user
 *       description: The user to create.
 *       schema:
 *       type: object
 *       required:
 *           - name
 *           - email
 *           - phone
 *           - website
 *           - address
 *       properties:
 *           name:
 *           type: string
 *           email:
 *           type: string
 *           phone:
 *           type: string
 *           website :
 *           type : string
 *           address
 *           type : address
 *              
 */
router.post("/company/create", CompanyController.create);

/**
 * @swagger
 * /api/company/{id}:
 *   get:
 *     tags:
 *       - Companies
 *     description: Returns a single company
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: company's id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A single puppy
 *         schema:
 *           $ref: '#/definitions/Puppy'
 */
router.get("/company/:id", CompanyController.get);

/**
 * @swagger
 * definitions:
 *   Company:
 *     properties:
 *       reports:
 *         type: array
 *       _id:
 *         type: string
 *       name:
 *         type: string
 *       email:
 *         type: string
 *       phone:
 *         type: string
 *       address:
 *         type: string
 *       website:
 *         type: string
 *       createdAt:
 *         type: date
 *       updatedAt:
 *         type: date
 */
/**
 * @swagger
 * /api/companies:
 *   get:
 *     tags:
 *       - Companies
 *     description: Returns all companies
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of companies
 *         schema:
 *           $ref: '#/definitions/Company'
 */
router.get("/companies", CompanyController.index);
//search company reports


// REPORT
router.get("/reports", ReportController.index);
router.post("/report/create", ReportController.create);
//search report by report type
router.get("/report?", ReportController.reportByType);
// router.get("/report?", ReportController.reportByType);




module.exports = router;

