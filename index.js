"use strict";
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const dotenv = require("dotenv").config();
let router = require("./router");
var swaggerJSDoc = require('swagger-jsdoc');

mongoose.connect(process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

//instantiate connection to MongoDB
let db = mongoose.connection;
db.on("error", console.error.bind(console, "An error occured while connecting to the database with error :"));

const app = express();
// swagger definition
var swaggerDefinition = {
  info: {
    title: 'Stears Project Swagger API',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: 'localhost:3000',
  basePath: '/',
};


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

 // options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs
  apis: ['./routes/*.js'],
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.use("/api", router);
// serve swagger
app.get('/swagger.json', function(req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.listen(process.env.APP_PORT || 5000);
