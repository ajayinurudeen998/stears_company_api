const Report = require("../models/Report.js");
const Company = require("../models/Company.js");

exports.get = (req, res) => {
	Report.findById(req.params.id, function(err, report) {
	    if (err || !report) {
	    	return res.status(404).json({ success: false, message: 'Report not found.' });
	    } 

	    return res.status(200).json({ success: true, report });
	});
}

exports.index = (req, res) => {
	Report.find().populate('company').exec(function(err, reports) {

	    if (err || !reports) {
	    	return res.status(404).json({ success: false, message: 'No report record found!' });
	    } 

	    return res.status(200).json({ success: true, reports });
	});
}

// { $year: new Date("2016-01-01") }
exports.create = (req, res) => {
	let report = new Report();
    report.name = req.body.name;
	report.type = req.body.type;
	report.period = req.body.period;
	report.year = req.body.year;
	report.assignee = req.body.assignee;
	report.deadline = new Date(req.body.deadline);
	report.submitted = req.body.submitted;
    report.url = req.body.url;
    report.company = req.body.company_id;
    

    report.save((err, report_response) => {
		if (err) {
            console.log(err);
    		return res.status(404).json({ status: false, message: 'An error occured ' });
        }

        //push saved report into company's report array
        Company.findOneAndUpdate((
            { _id : req.body.company_id },
            { $push : { reports : report_response._id }}), (err, result) => {
                if(err) {
                    // throw err
            		return res.status(404).json({ status: false, message: err });

                };
    		    return res.status(200).json({ status: false, message: 'All good! ' });

            	return res.status(200).json({ success: true, report_response });
                
            }
        )
        
	});
}

exports.reportByType = (req, res) => {
    // return res.status(404).json({ success: false, message: req.query.type });
    if (req.query.companyId) {
        let company_reports = Report.find({company: req.query.companyId}).select('name createdAt assignee deadline').exec(function(err, company_reports) {
            if (err) {
                return res.status(404).json({ success: false, message: 'An error occured' });
            }
        });
        return res.status(200).json({ success: true, data: { company_reports } });
    }
    let reports = Report.find({type: new RegExp('^'+req.query.type+'$', "i")}, (err, reports) => {
		if (err) {
			return res.status(404).json({ success: false, message: 'An error occured' });
		}
	    return res.status(200).json({ success: true, data: { reports } });
	})
}

