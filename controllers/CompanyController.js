const Company = require("../models/Company.js");
// const Report = require("../models/Report.js");


exports.get = (req, res) => {
	Company.findById(req.params.id).populate('reports').exec(function(err, company) {
	    if (err || !company) {
	    	return res.status(404).json({ success: false, message: 'Company not found.' });
        } 

	    return res.status(200).json({ success: true, company });
	});
}

exports.index = (req, res) => {
	Company.find().populate('reports').exec(function(err, companies) {
        
	    if (err || !companies) {
	    	return res.status(404).json({ success: false, message: 'No company record found!' });
	    } 

	    return res.status(200).json({ success: true, companies });
	});
}

exports.create = (req, res) => {
	let company = new Company();
	company.name = req.body.name;
    company.email = req.body.email;
	company.phone = req.body.phone;
    company.address = req.body.address;
    company.website = req.body.website;
	company.description = req.body.description;

    company.save((err, company_response) => {
		if (err) {
            console.log(err);
    		return res.status(404).json({ status: false, message: 'An error occured '.err });
    	}
    	return res.status(200).json({ success: true, company_response });
	});
}

